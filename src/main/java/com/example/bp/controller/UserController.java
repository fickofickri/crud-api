package com.example.bp.controller;


import com.example.bp.dto.UserRequest;
import com.example.bp.dto.UserUpdate;
import com.example.bp.model.User;
import com.example.bp.repository.UserRepository;
import com.example.bp.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/user")
public class UserController {

    Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Operation(summary = "Create User", description = "", tags = { "USER API" })
    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody User user){

        Map<String, Object> response = new LinkedHashMap<String, Object>();
        HttpStatus httpStatus = HttpStatus.OK;

        String status;
        String message;
        String username = null;
        Date dateNow = Calendar.getInstance().getTime();

        try {
//            List<User> userList = userService.findByUserid(user.getUserid());

                userService.setDataUser(user);
                response.put("status", "Success");
                response.put("message", "Data Berhasil Disave");
                response.put("time", LocalDateTime.now());
                response.put("data", user);


        } catch (IOException e) {
            log.error("## ERROR : " + e.getMessage());
            e.printStackTrace();
            response.put("status", Boolean.FALSE);
            response.put("time", LocalDateTime.now());
            response.put("message", e.getMessage());
            response.put("data", null);
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response, httpStatus);
    }

    @Operation(summary = "Update user by ID", description = "", tags = { "USER API" })
    @PostMapping("update/{userid}")
    public ResponseEntity updateUser(@PathVariable("userid") Integer userid, @RequestBody UserUpdate userRequest){

        Map<String, Object> response = new LinkedHashMap<String, Object>();
        HttpStatus httpStatus = HttpStatus.OK;

        try {

            User user = userService.updateUserById(userid, userRequest);

            response.put("status", Boolean.TRUE);
            response.put("message", "success");
            response.put("time", LocalDateTime.now());
            response.put("data", user);

            return new ResponseEntity<>(response, httpStatus);

        } catch (Exception e) {

            log.error("## ERROR : " + e.getMessage());
            e.printStackTrace();
            response.put("status", Boolean.FALSE);
            response.put("time", LocalDateTime.now());
            response.put("message", e.getMessage());
            response.put("data", null);
            httpStatus = HttpStatus.BAD_REQUEST;

        }

        return new ResponseEntity<>(response, httpStatus);
    }


//    @Operation(summary = "getAllData User", description = "", tags = { "USER API" })
//    @GetMapping
//    public  List<User> getAllUsers() {
//
//        return userService.getAllUser();
//
//    }

    @Operation(summary = "Get User", description = "", tags = { "USER API" })
    @GetMapping
    public  List<UserRequest> getAllUsersReq() {

        return userService.getAllUserRequest();

    }


    @Operation(summary = "Delete User By ID User", description = "", tags = { "USER API" })
    @DeleteMapping("delete/{userid}")
    public ResponseEntity<?> deleteUser(@PathVariable("userid") Integer userid){

        Map<String, Object> response = new LinkedHashMap<String, Object>();
        HttpStatus httpStatus = HttpStatus.OK;

        userRepository.deleteById(userid);

        //String message = "User ID tidak terdaftar";

        response.put("status", Boolean.TRUE);
        response.put("message", "Data Berhasil Dihapus");
        response.put("time", LocalDateTime.now());


        return new ResponseEntity<>(response, httpStatus);
    }





}
