package com.example.bp.repository;

import com.example.bp.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findByNamaIgnoreCaseContaining(String nama);

    @Query(value = " SELECT * FROM product WHERE nama LIKE %:nama% ", nativeQuery = true)
    List<Product> findbyNama(String nama);

    @Query(value = " SELECT * FROM product WHERE harga LIKE %:harga% ", nativeQuery = true)
    List<Product> findbyHarga(String harga);

    @Query(value = "SELECT MAX(id) FROM product", nativeQuery = true)
    Long findLastNomorRegister();
}
