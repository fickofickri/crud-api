package com.example.bp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cart")
public class Cart implements Serializable {


    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "jml_pesanan")
    private String jml_pesanan;

    @Column(name = "keterangan")
    private String keterangan;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJml_pesanan() {
        return jml_pesanan;
    }

    public void setJml_pesanan(String jml_pesanan) {
        this.jml_pesanan = jml_pesanan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }


        public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("jml_pesanan", jml_pesanan)
                .append("product", product)
                .append("keterangan", keterangan)
                .append("order", order)
                .toString();
    }
}
