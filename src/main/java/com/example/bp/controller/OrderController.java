package com.example.bp.controller;

import com.example.bp.model.Order;
import com.example.bp.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/order")
public class OrderController {

    Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    OrderService orderService;


    //GET CART

    @Operation(summary = "Get Order", description = "", tags = { "ORDER API" })
    @GetMapping("/findorder")
    public ResponseEntity<List<Order>> getAllPesanans() {

        List<Order> pesanans = orderService.getAllOrder();
        return new ResponseEntity<>(pesanans, HttpStatus.OK);

    }

}
