package com.example.bp.repository;

import com.example.bp.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {


//    @Query(value = "SELECT * FROM cart", nativeQuery = true)
//    List<Cart> findCart();

}
