package com.example.bp.dto.cart;


import com.example.bp.model.Product;

import java.io.Serializable;

public class CartDTO implements Serializable {


    private Integer id;
    private String jml_pesanan;
    private String keterangan;
    private Product product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJml_pesanan() {
        return jml_pesanan;
    }

    public void setJml_pesanan(String jml_pesanan) {
        this.jml_pesanan = jml_pesanan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
