package com.example.bp.service;

import com.example.bp.model.Product;
import com.example.bp.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    Logger log = LoggerFactory.getLogger(ProductService.class);

    //Generate ID
    @Transactional
    public String generateId(){

        String id = String.valueOf(productRepository.findLastNomorRegister());

        try {

            if (id != null) {
                Long nomorRegis = productRepository.findLastNomorRegister() + 1;
                id = String.valueOf(nomorRegis);
            }

        }catch (Exception e){
            log.debug("Exception : " + e.getMessage());
            e.printStackTrace();
        }

        return id;
    }

    // SET DATA PRODUCT
    @Transactional
    public Product setDataProduct(Product product) throws IOException{
        product.setId(Integer.valueOf(generateId()));
        product.setKode(product.getKode());
        product.setNama(product.getNama());
        product.setHarga(product.getHarga());
        product.setActive(product.getActive());
        product.setGambar(product.getGambar());

        return productRepository.save(product);
    }

    // FIND PRODUCT

//    public List<Product> findProduct(String keyword) {
//
//        return productRepository.findByNamaIgnoreCaseContaining(keyword);
//    }

}
