1. Sebelum menjalankan aplikasi dilokal pastikan project sudah diclone lalu restore/import database postgre bernama db_bp.sql
2. Buka pgadmin lalu buat nama database dengan nama db_bp 
3. Import file db_bp.sql  
4. Jika restore sudah selesai jalankan aplikasi springboot 
5. Buka localhost/11001/bp maka akan tampil swagger UI
