package com.example.bp.service;

import com.example.bp.dto.UserRequest;
import com.example.bp.dto.UserUpdate;
import com.example.bp.model.User;
import com.example.bp.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    Logger log = LoggerFactory.getLogger(UserService.class);

//    CREATE
    @Transactional
    public User setDataUser(User user) throws IOException {
        user.setUserid(user.getUserid());
        user.setNamalengkap(user.getNamalengkap().toUpperCase());
        user.setUsername(user.getUsername().toLowerCase());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setStatus(user.getStatus());
        user.setIsActive(user.getIsActive());

        return userRepository.save(user);
    }

//    @Transactional
//    public List<User> findAll(User user) {
//        user.getUserid();
//        user.getNamalengkap();
//        user.getUsername();
//        user.getPassword();
//
//
//        return userRepository.findAll();
//    }

//    @Transactional
//    public List<User> getAllUser() {
//
//        User userRequest = new User();
//        List<User>  userRequestList = userRepository.findUsers();
//        List<User> newUserList = new ArrayList<>();
//
//        for (User userRequestData : userRequestList) {
//            userRequest.setUserid(userRequestData.getUserid());
//            userRequest.setNamalengkap(userRequestData.getNamalengkap());
//            userRequest.setUsername(userRequestData.getUsername());
//            userRequest.setStatus(userRequestData.getStatus());
//            newUserList.add(userRequest);
//        }
//
//        return newUserList;
//    }


//    UPDATE
    @Transactional
    public User updateUserById (Integer userid, UserUpdate userUpdate) {

    Optional<User> usersOptional = userRepository.findById(userid);
    User user = usersOptional.get();
    if (user != null) {

        user.setNamalengkap(userUpdate.getNamalengkap().toUpperCase());
        user.setUsername(userUpdate.getUsername());
        user.setPassword(passwordEncoder.encode(userUpdate.getPassword()));
        userRepository.save(user);

     }
        return user;
    }


    @Transactional
    public List<UserRequest> getAllUserRequest() {

        UserRequest userRequest = null;
        List<User>  userRequestList = userRepository.findUsers();
        List<UserRequest> newUserList = new ArrayList<>();

        for (User userRequestData : userRequestList) {
            userRequest = new UserRequest();
            userRequest.setUserid(userRequestData.getUserid());
            userRequest.setNamalengkap(userRequestData.getNamalengkap());
            userRequest.setUsername(userRequestData.getUsername());
            userRequest.setStatus(userRequestData.getStatus());
            newUserList.add(userRequest);
        }

        return newUserList;
    }

//   DELETE
    @Transactional
    public void deleteByUserid(Integer userid) {

        userRepository.deleteById(userid);
    }

}
