package com.example.bp.controller;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@Tag(name = "Home", description = "Default")
@RestController
@RequestMapping(path = "")
public class HomeController {

    Logger log = LoggerFactory.getLogger(HomeController.class);

    @Hidden
    @GetMapping
    public RedirectView hello()
    {
        return new RedirectView("./swagger-ui.html");
    }

}

