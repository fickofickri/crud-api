package com.example.bp.controller;

import com.example.bp.dto.cart.CartDTO;
import com.example.bp.repository.CartRepository;
import com.example.bp.service.CartService;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/cart")
public class CartController {

    Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    CartRepository cartRepository;

    @Autowired
    CartService cartService;

    //GET CART

    @Operation(summary = "Get Cart", description = "", tags = { "CART API" })
    @GetMapping("/findcart")
    public ResponseEntity<List<CartDTO>> getCart() {

        List<CartDTO> pesanans = cartService.getAllCart();
        return new ResponseEntity<>(pesanans, HttpStatus.OK);

    }

}
