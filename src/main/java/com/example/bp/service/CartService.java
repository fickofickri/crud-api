package com.example.bp.service;

import com.example.bp.dto.cart.CartDTO;
import com.example.bp.model.Cart;
import com.example.bp.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {

    @Autowired
    CartRepository cartRepository;
    @Transactional
    public List<CartDTO> getAllCart(){

        List<Cart> carts = cartRepository.findAll();
        List<CartDTO> cartDTOS = new ArrayList<>();

        for(Cart cart : carts){
            CartDTO cartDTO = new CartDTO();
            cartDTO.setId(cart.getId());
            cartDTO.setJml_pesanan(cart.getJml_pesanan());
            cartDTO.setKeterangan(cart.getKeterangan());
            cartDTO.setProduct(cart.getProduct());

            cartDTOS.add(cartDTO);
        }

        return cartDTOS;
    }
}
