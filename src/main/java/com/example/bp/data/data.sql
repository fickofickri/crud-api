

-- create New Database dengan nama db_bp

-- TABLE cart;

CREATE TABLE cart (
	id int4 NULL,
	jml_pesanan int4 NULL,
	product_id int4 NULL,
	keterangan varchar NULL,
	subtotal_price int4 NULL,
	order_id int4 NULL
);


-- TABLE orders;

CREATE TABLE orders (
	id int4 NULL,
	nama varchar NULL,
	"date" timestamp NULL,
	total_price int4 NULL
);


--  TABLE product;

CREATE TABLE product (
	id int4 NULL,
	kode varchar NULL,
	nama varchar NULL,
	harga numeric NULL,
	is_active bool NULL,
	gambar varchar NULL
);


--  TABLE tbl_user;

CREATE TABLE tbl_user (
	user_id int8 NOT NULL,
	nama_lengkap varchar(255) NULL,
	"password" varchar(255) NULL,
	status varchar(255) NULL,
	username varchar(255) NULL,
	is_active varchar(255) NULL,
	CONSTRAINT tbl_user_pkey PRIMARY KEY (user_id)
);