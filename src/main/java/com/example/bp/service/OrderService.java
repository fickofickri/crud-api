package com.example.bp.service;

import com.example.bp.model.Order;
import com.example.bp.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Transactional
    public List<Order> getAllOrder(){

        List<Order> orders = orderRepository.findAll();
//        List<OrderDTO> orderDTOS = new ArrayList<>();
//
//        for(Order order : orders){
//            OrderDTO orderDTO = new OrderDTO();
//            orderDTO.setId(order.getId());
//            orderDTO.setNama(order.getNama());
//            orderDTO.setDate(order.getDate());
//            orderDTO.setCart(order.getCart());
//
//            orderDTOS.add(orderDTO);
//        }

        return orders;
    }



}
