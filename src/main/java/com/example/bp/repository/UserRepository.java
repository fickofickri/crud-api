package com.example.bp.repository;

import com.example.bp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findByUserid(Integer userid);

    @Query(value = "SELECT * FROM tbl_user", nativeQuery = true)
    List<User> findUsers();

}
