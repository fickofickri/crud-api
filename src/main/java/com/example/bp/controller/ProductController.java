package com.example.bp.controller;

import com.example.bp.model.Product;
import com.example.bp.repository.ProductRepository;
import com.example.bp.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/product")
public class ProductController {
    Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductService productService;

    //CREATE PRODUCT

    @Operation(summary = "Create Product", description = "", tags = { "PRODUCT API" })
    @PostMapping("/create")
    public ResponseEntity<?> createProduct(@RequestBody Product product){

        Map<String, Object> response = new LinkedHashMap<String, Object>();
        HttpStatus httpStatus = HttpStatus.OK;

        try {
            productService.setDataProduct(product);
            response.put("status", "Success");
            response.put("message", "Data Berhasil Disave");
            response.put("time", LocalDateTime.now());
            response.put("data", product);

        } catch (IOException e) {
            log.error("## ERROR : " + e.getMessage());
            e.printStackTrace();
            response.put("status", Boolean.FALSE);
            response.put("time", LocalDateTime.now());
            response.put("message", e.getMessage());
            response.put("data", null);
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response, httpStatus);
    }

    //GET PRODUCT

    @Operation(summary = "Get Product", description = "", tags = { "PRODUCT API" })
    @GetMapping
    public List<Product> getAllProduct() {

        return productRepository.findAll();

    }

    //FIND PRODUCT

    @Operation(summary = "Find Product", description = "", tags = { "PRODUCT API" })
    @GetMapping("/find")
    public List<Product> searchProducts(@RequestParam String nama) {

        return productRepository.findByNamaIgnoreCaseContaining(nama);
    }

}
